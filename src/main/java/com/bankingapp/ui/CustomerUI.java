package com.bankingapp.ui;

import com.bankingapp.business.*;
import com.bankingapp.entities.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.DecimalFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.*;

public class CustomerUI {
    private CustomerService customerService;
    private AccountService accountService;
    private LoanService loanService;

    private LoanUI loanUI;

    private Logger logger = LogManager.getLogger(CustomerUI.class);

    public CustomerUI(CustomerService customerService, AccountService accountService, LoanService loanService, LoanUI loanUI) {
        this.customerService = customerService;
        this.accountService = accountService;
        this.loanService = loanService;

        this.loanUI = loanUI;
    }

    public void signUp() {
        createCustomerFromPrompt();
        System.out.println("Signup successful");
        System.out.println("Log into your new user account");
        System.out.println();
        logIn();
    }

    private Customer createCustomerFromPrompt() {

        String name = Console.getInputString("Enter name");

        String userName = Console.getInputString("Enter username?");

        String password = Console.getInputString("Enter password");

        String address = Console.getInputString("Enter Address");

        String email = Console.getInputString("Email Address");

        Customer customer = null;
        try {
            customer = customerService.createNewCustomer(userName, password, name, email, address);

            logger.debug("new customer created successfully");
        } catch (BusinessException e) {
            logger.error("failed to create new account", e);
        }

        return customer;
    }

    public void logIn() {

        String userName = Console.getInputString("Enter userName");

        String password = Console.getInputString("Enter password");

        try {

            Customer customer = customerService.findCustomer(userName, password);
            logger.debug("Log in successfully");

            if (customer != null) {
                System.out.println("Logged in successfully!");
                startCustomerActivities(customer);
            } else {
                System.out.println("Log in failed! Try again");
                logIn();
            }

        } catch (BusinessException e) {
            logger.error("failed to log in");
        }
    }

    private void startCustomerActivities(Customer customer) throws BusinessException {

        loop:
        while (true) {
            System.out.println();
            System.out.println("What will you like to do today, " + customer.getCustomerName());
            System.out.println("1.Open a new account");
            System.out.println("2.View existing account");
            System.out.println("3.View transaction");
            System.out.println("4.Transfer funds");
            System.out.println("5.Withdrawal funds");
            System.out.println("6.Cash Deposit");
            System.out.println("7.View Personal Information");
            System.out.println("8.Make Loan Payment");
            System.out.println("9.log out");

            int userResponse = Console.getResponse(1, 9);

            switch (userResponse) {
                case 1:
                    openNewAccount(customer);
                    break;
                case 2:
                    viewCustomerAccount(customer);
                    break;
                case 3:
                    ViewAllTransactions(customer);
                    break;
                case 4:
                    transferFunds(customer);
                    break;
                case 5:
                    withdrawFunds(customer);
                    break;
                case 6:
                    depositMoney(customer);
                    break;
                case 7:
                    viewInformation(customer);
                    break;
                case 8:
                    loanUI.makeloanPayment(customer);
                    break;
                case 9:
                    System.out.println("Thanks for visiting today!");
                    break loop;
            }
        }
        System.out.println();
    }

    private void openNewAccount(Customer customer) throws BusinessException {

        List<AccountType> accountTypes = accountService.getAllAccountTypes();
        logger.debug("account type selected successfully");

        System.out.println("Select account type.");

        for (int i = 0; i < accountTypes.size(); i++) {
            AccountType accountType = accountTypes.get(i);
            int optionNumber = i + 1;
            System.out.println(optionNumber + ". " + accountType.getAccountTypeName());
        }

        int customerResponse = Console.getResponse(1, accountTypes.size());

        AccountType accountType = accountTypes.get(customerResponse - 1);
        System.out.println(accountType.getDescription());
        Customer partner = null;

        if (customerResponse == 3 || customerResponse == 4 || customerResponse == 6) {
            System.out.println("Is your partner here with you? Enter their details");
            partner = createCustomerFromPrompt();
        }

        //Auto Loan or Joint Auto Loan
        if (customerResponse == 5 || customerResponse == 6) {
            loanUI.openLoanAccount(customer, partner, accountType);
        } else {
            double amount = Console.getAmountOfMoney("How much money will you deposit today?");
            accountService.createNewAccount(accountType, amount, customer, partner);
        }
    }

    private Account getAccountForCustomerActivity(Customer customer, String prompt, boolean includeLoans) throws BusinessException {
        List<Account> accountList = accountService.findAccountsOfCustomer(customer);
        logger.debug("account of customer successfully received");

        DecimalFormat formatter = new DecimalFormat("$###,###.##");

        System.out.println(prompt);

        for (int i = 0; i < accountList.size(); i++) {
            int accountOption = i + 1;
            Account account = accountList.get(i);
            if(!includeLoans && account.getAccountType().getAccountTypeName().contains("Loan"))
                continue;
            System.out.println(accountOption + ". " + account.getAccountType().getAccountTypeName() + " Account #:" + account.getAccountNumber() + " Balance: " + formatter.format(account.getAccountBalance()));
        }

        int customerResponse = Console.getResponse(1, accountList.size());
        return accountList.get(customerResponse - 1);
    }

    private void viewCustomerAccount(Customer customer) throws BusinessException {
        Account account = getAccountForCustomerActivity(customer, "Which account would like to view?", true);
        logger.debug("Account balance viewed successfully");
        System.out
                .println("The balance on account " + account.getAccountNumber() + " is " + account.getAccountBalance());
    }

    private void depositMoney(Customer customer) throws BusinessException {

        Account account = getAccountForCustomerActivity(customer, "Which account would like to deposit money into?",false);

        double amount = Console.getAmountOfMoney("How much money would you like to deposit?");

        accountService.depositMoney(account, customer, amount);

        System.out.println(amount + " has successfully been credited to your account.");
        System.out.println("Your new balance is " + account.getAccountBalance());
    }

    private void withdrawFunds(Customer customer) throws BusinessException {

        Account account = getAccountForCustomerActivity(customer, "Which account would like to withdraw money from?", false);

        double amount = Console.getAmountOfMoney("How much money would you like to withdraw?");
        accountService.withdrawMoney(account, customer, amount);

        double newBalance = account.getAccountBalance() - amount;
        if (newBalance >= 0) {
            accountService.withdrawMoney(account, customer, amount);
            System.out.println(amount + " has successfully been withdrawn from your account.");
            System.out.println("Your new balance is " + newBalance);
        } else {
            System.out.println("You may not overdraw this account");
        }
    }

    private void transferFunds(Customer customer) throws BusinessException {
        Account sourceAccount = getAccountForCustomerActivity(customer,
                "Which account would like to transfer money from?", false);

        Account targetAccount = getAccountForCustomerActivity(customer,
                "Which account would like to transfer money to?", true);

        double amount = Console.getAmountOfMoney("How much money would you like to transfer?");

        double newSourceBalance = sourceAccount.getAccountBalance() - amount;
        if (newSourceBalance >= 0) {
            accountService.transferMoney(sourceAccount, targetAccount, customer, amount);

            if(targetAccount.getAccountType().getAccountTypeName().contains("Loan")) {
                loanService.savePayment(customer, targetAccount, amount);
            }
            System.out.println(amount + " has successfully been transferred from "
                    + sourceAccount.getAccountType().getAccountTypeName() +
                    " Account " + sourceAccount.getAccountNumber()
                    + " to " + targetAccount.getAccountType().getAccountTypeName() +
                    " Account " + targetAccount.getAccountNumber());
        } else {
            System.out.println("Insufficient funds in the source account");
        }
    }

    private void viewInformation(Customer customer) throws BusinessException {

        System.out.println("Name: " + customer.getCustomerName());
        System.out.println("Address: " + customer.getAddress());
        System.out.println("Email: " + customer.getEmail());
        System.out.println("Here are a list of your accounts");

        List<Account> accounts = accountService.findAccountsOfCustomer(customer);

        for (Account account : accounts) {
            System.out.print("Account Number: " + account.getAccountNumber());
            System.out.print(" ");
            System.out.print("Balance: " + account.getAccountBalance());
            System.out.println();
        }
    }

    private void ViewAllTransactions(Customer customer) throws BusinessException {
        List<AccountTransaction> transactions = accountService.getAllTransactionByCustomer(customer);
        if (transactions.size() > 0) {
            logger.debug("customer has " + transactions.size() + " transactions");
            for (AccountTransaction accountTransaction : transactions) {
                System.out.print("Date: ");
                System.out.print(accountTransaction.getDate().atZone(ZoneId.systemDefault())
                        .format(DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG)));
                System.out.print(" Account: ");
                System.out.print(accountTransaction.getTargetAccount().getAccountNumber());
                System.out.print(" Amount: ");
                System.out.print(accountTransaction.getAmount());
                System.out.println();
            }
        } else {
            logger.debug("customer has no " + transactions.size() + " transactions");
            System.out.println("Customer has no transactions");
        }
        System.out.println();
    }

}
