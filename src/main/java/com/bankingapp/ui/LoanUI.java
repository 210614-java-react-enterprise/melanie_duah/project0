package com.bankingapp.ui;

import com.bankingapp.business.AccountService;
import com.bankingapp.business.BusinessException;
import com.bankingapp.business.CustomerService;
import com.bankingapp.business.LoanService;
import com.bankingapp.entities.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.DecimalFormat;
import java.util.List;

public class LoanUI {
    private AccountService accountService;
    private LoanService loanService;
    private CustomerService customerService;

    private Logger logger = LogManager.getLogger(CustomerUI.class);

    public LoanUI(AccountService accountService, CustomerService customerService, LoanService loanService) {
        this.accountService = accountService;
        this.loanService = loanService;
        this.customerService = customerService;
    }

    public void openLoanAccount(Customer customer, Customer partner, AccountType accountType) throws BusinessException {
        boolean isValidEmployment = verifyCustomerEmployment(customer);
        if (isValidEmployment) {
            LoanTerm loanTerm = getCustomerSelectedLoanTerm(customer);
            double vehicleCost = Console.getAmountOfMoney("What's the total cost of the vehicle (incl taxes and fees)?");
            double downPayment = Console.getAmountOfMoney("How much down payment would you like to make?");
            double principal = vehicleCost - downPayment;
            double paymentWithoutInterest = principal / loanTerm.getTerm();
            double interest = (loanTerm.getInterestRate() / 100) * paymentWithoutInterest;
            double monthlyPayment = paymentWithoutInterest + interest;

            Account account = accountService.createNewAccount(accountType, principal, customer, partner);
            loanService.saveLoanAccount(account, loanTerm, principal, monthlyPayment, downPayment);

            System.out.println("You are set! Enjoy your new ride!");
        }
    }

    private LoanTerm getCustomerSelectedLoanTerm(Customer customer) throws BusinessException {
        List<LoanTerm> loanTerms = loanService.getLoanTerms();
        logger.debug("successfully loaded loan terms");

        System.out.println("Select Loan Term and Rate");
        for (int i = 0; i < loanTerms.size(); i++) {
            LoanTerm loanTerm = loanTerms.get(i);
            int optionNumber = i + 1;

            System.out.println(optionNumber + ". " + loanTerm.getTerm() + " months  " + loanTerm.getInterestRate() + "% APR");
        }
        int customerResponse = Console.getResponse(1, loanTerms.size());

        return loanTerms.get(customerResponse - 1);
    }

    private boolean verifyCustomerEmployment(Customer customer) throws BusinessException {
        boolean isValidEmployment = false;
        List<EmploymentStatus> employmentStatuses = customerService.getAllEmploymentStatus();
        logger.debug("successfully loaded employment statuses");

        System.out.println("Select Employment Status");
        for (int i = 0; i < employmentStatuses.size(); i++) {
            EmploymentStatus employmentStatus = employmentStatuses.get(i);
            int optionNumber = i + 1;

            System.out.println(optionNumber + ". " + employmentStatus.getEmploymentStatusName());
        }
        int customerResponse = Console.getResponse(1, employmentStatuses.size());

        EmploymentStatus employmentStatus = employmentStatuses.get(customerResponse - 1);

        if (customerResponse == 4 || customerResponse == 5) {
            System.out.println("We are afraid we cannot process your application at this time based on your employment status");
        } else {
            saveCustomerEmploymentDetails(customer, employmentStatus);
            isValidEmployment = true;
        }

        return isValidEmployment;
    }

    private void saveCustomerEmploymentDetails(Customer customer, EmploymentStatus employmentStatus) throws BusinessException {
        String employerName = "";
        String jobTitle = "";
        int employmentDuration = -1;
        double annualIncome = 0;
        String phoneNumber = null;

        logger.debug("successfully customer employment details");
        if (employmentStatus.getEmploymentStatusName().equals("Employed")) {
            employerName = Console.getInputString("Enter your Employer name: ");
            phoneNumber = Console.getInputString("Employer contact Number");
        } else if (employmentStatus.getEmploymentStatusName().equals("Self-employed")) {
            employerName = "Self";
        }

        if (employmentStatus.getEmploymentStatusName().equals("Employed") ||
                employmentStatus.getEmploymentStatusName().equals("Self-employed")) {
            jobTitle = Console.getInputString("Job Title");
            employmentDuration = Console.getInputInt("How long have you worked as a " + jobTitle + " (approximate in years)");
        }

        annualIncome = Console.getAmountOfMoney("Annual Income");
        customerService.saveCustomerEmployment(customer, employmentStatus, employerName, jobTitle,
                employmentDuration, annualIncome, phoneNumber);
    }

    public void makeloanPayment(Customer customer) throws BusinessException {
        LoanAccount loanAccount = getCustomerSelectedLoanAccount(customer);
        logger.debug("successfully made payment");
        double paymentAmount = 0;

        while (paymentAmount < Math.round(loanAccount.getMonthlyPayment())) {
            paymentAmount = Console.getAmountOfMoney("Enter an amount equal to or higher than the monthly payment:");
        }
        loanService.savePayment(customer, loanAccount, paymentAmount);

        System.out.println("Payment successfully made to your loan account. New balance is "
                + new DecimalFormat("$###,###.##").format(loanAccount.getAccountBalance()));

    }

    private LoanAccount getCustomerSelectedLoanAccount(Customer customer) throws BusinessException {
        LoanAccount loanAccount = null;

        List<LoanAccount> loanAccounts = loanService.findLoanAccountsOfCustomer(customer);
        logger.debug("successfully customer loan accounts");
        DecimalFormat formatter = new DecimalFormat("$###,###.##");

        if (loanAccounts.size() > 1) {

            System.out.println("Select Loan account to pay to");
            for (int i = 0; i < loanAccounts.size(); i++) {
                LoanAccount account = loanAccounts.get(i);
                int optionNumber = i + 1;

                System.out.println(optionNumber + ". Account #:" + account.getAccountNumber() + " Principal: " + formatter.format(account.getPrincipal()) + " Due monthly: " + formatter.format(account.getMonthlyPayment()) + " Balance: " + formatter.format(account.getAccountBalance()));
            }
            int customerResponse = Console.getResponse(1, loanAccounts.size());

            loanAccount = loanAccounts.get(customerResponse - 1);
        } else {
            loanAccount = loanAccounts.get(0);
            System.out.println("Account #:" + loanAccount.getAccountNumber() + " Principal: " + formatter.format(loanAccount.getPrincipal()) + " Due monthly: " + formatter.format(loanAccount.getMonthlyPayment()) + " Balance: " + formatter.format(loanAccount.getAccountBalance()));
        }

        return loanAccount;
    }
}
