package com.bankingapp.ui;

import com.bankingapp.business.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
    public static void main(String[] args) {
        Logger logger = LogManager.getLogger(Main.class);
        logger.info("Application started");

        try {
            CustomerService customerService = BusinessServiceCreator.getCustomerService(); //factory
            AccountService accountService = BusinessServiceCreator.getAccountService();
            LoanService loanService = BusinessServiceCreator.getLoanService();

            LoanUI loanUI = new LoanUI(accountService,customerService, loanService);
            CustomerUI customerUI = new CustomerUI(customerService, accountService, loanService, loanUI);

            loop:
            while (true) {
                System.out.println("Welcome to Bank of Chevy");
                System.out.println("What can we do for you today?");

                System.out.println("1.Login as Customer");
                System.out.println("2.Sign up as a new Customer");
                System.out.println("3.Quit");

                int userResponse = Console.getResponse(1, 4);

                switch (userResponse) {
                    case 1:
                        customerUI.logIn();
                        break;
                    case 2:
                        customerUI.signUp();
                        break;
                    case 3:
                        System.out.println("Thanks for stopping by. Hope to see you soon");
                        break loop;
                }
            }
        }catch (BusinessException e){
            System.out.println("Something happened on our end. Please try again");
        }
    }
}
