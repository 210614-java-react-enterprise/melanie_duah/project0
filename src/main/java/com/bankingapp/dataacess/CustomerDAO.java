package com.bankingapp.dataacess;

import com.bankingapp.entities.*;

import java.util.List;

public interface CustomerDAO {
    void saveCustomer(String userName, String password, String name, String email, String address) throws DAOException;

    Customer findCustomer(String userName, String password) throws DAOException;

    List<Customer> findAllCustomers() throws DAOException;

    List<Customer> findCustomersBy(Account account) throws DAOException;

    List<EmploymentStatus> getEmploymentStatus() throws DAOException;

    void saveCustomerEmployment(CustomerEmployment customerEmployment) throws DAOException;
}
