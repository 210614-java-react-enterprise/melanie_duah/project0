package com.bankingapp.dataacess;

import com.bankingapp.entities.Account;
import com.bankingapp.entities.Customer;
import com.bankingapp.entities.LoanAccount;
import com.bankingapp.entities.LoanTerm;
import org.apache.logging.log4j.*;

import java.sql.Connection;
import java.sql.*;
import java.sql.SQLException;
import java.util.*;

public class LoanDAOImpl implements LoanDAO{
    private Logger logger=LogManager.getLogger(LoanDAOImpl.class);
    private Connection connection;
    public LoanDAOImpl(Connection connection) {
        this.connection=connection;
    }

    @Override
    public List<LoanTerm> getLoanTerms() throws DAOException {
        List<LoanTerm>loanTerms=new ArrayList<>();
        try{
            String sql="SELECT loan_rateid,term,interestrate FROM loan_rate";
            PreparedStatement preparedStatement=connection.prepareStatement(sql);
            ResultSet result=preparedStatement.executeQuery();

            logger.debug("Select Terms and its Rate");

            while (result.next()){
                int loanRateId=result.getInt(1);
                int term=result.getInt(2);
                double interestRate=result.getDouble(3);

                LoanTerm loanTerm=new LoanTerm();

                loanTerm.setLoanTermId(loanRateId);
                loanTerm.setTerm(term);
                loanTerm.setInterestRate(interestRate);
                loanTerms.add(loanTerm);
            }

        } catch (SQLException e) {
            logger.error("failed to get Loan terms and rate", e);
            throw new DAOException("Failed to get loan terms and rate");
        }
        return loanTerms;
    }

    @Override
    public void saveLoanAccount(Account account, LoanTerm loanTerm, double principal, double monthlyPayment, double downPayment) throws DAOException {
        try{
            String sql = "INSERT INTO loan_account (account_id,loan_rateid,principal_amount,monthly_payment,down_payment) VALUES (?,?,?,?,?)";

            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, account.getAccountId());
            preparedStatement.setInt(2, loanTerm.getLoanTermId());
            preparedStatement.setDouble(3, principal);
            preparedStatement.setDouble(4, monthlyPayment);
            preparedStatement.setDouble(5, downPayment);

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error("Error saving loan account", e);
            throw new DAOException("Error saving loan account", e);
        }
    }
    
    @Override
    public List<LoanAccount> findLoanAccountsOfCustomer(Customer customer) throws DAOException {

        List<LoanAccount> loanAccounts = new ArrayList<LoanAccount>();
        try {
            String sql = "SELECT loan_accountid,account.account_id,account_number,account_balance,loan_rateid, principal_amount, monthly_payment, down_payment FROM account JOIN account_customer ON account.account_id =account_customer.account_id JOIN loan_account ON loan_account.account_id = account.account_id JOIN customer ON account_customer.customer_id =customer.customer_id WHERE customer.customer_id=?";

            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, customer.getCustomerId());

            ResultSet results = preparedStatement.executeQuery();

            logger.debug("Joining account information and account customer and customer successfully");

            while (results.next()) {

                int loanAccountId = results.getInt(1);
                int accountId = results.getInt(2);
                String accountNumber = results.getString(3);
                double accountBalance = results.getDouble(4);
                int loanRateId = results.getInt(5);
                double principal = results.getDouble(6);
                double monthlyPayment = results.getDouble(7);
                double downPayment = results.getDouble(8);

                LoanAccount account = new LoanAccount();

                account.setAccountId(accountId);
                account.setLoanAccountId(loanAccountId);
                account.setAccountNumber(accountNumber);
                account.setAccountBalance(accountBalance);
                LoanTerm loanTerm = new LoanTerm();
                loanTerm.setLoanTermId(loanRateId);
                account.setLoanTerm(loanTerm);
                account.setPrincipal(principal);
                account.setMonthlyPayment(monthlyPayment);
                account.setDownPayment(downPayment);

                loanAccounts.add(account);
            }

        } catch (SQLException e) {

            logger.error("Failed to find loan accounts of customer", e);
            throw new DAOException("Failed to find loan accounts of customer", e);
        }

        return loanAccounts;
    }

    @Override
    public void saveLoanPayment(Customer customer, LoanAccount loanAccount, double paymentAmount) throws DAOException {
        try{
            String sql = "INSERT INTO loan_payment (loan_accountid,customer_id,amount) VALUES (?,?,?)";

            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, loanAccount.getLoanAccountId());
            preparedStatement.setInt(2, customer.getCustomerId());
            preparedStatement.setDouble(3, paymentAmount);

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error("Error saving loan payment", e);
            throw new DAOException("Error saving loan payment", e);
        }
    }

    @Override
    public LoanAccount findLoanAccountByAccountId(int accountId) throws DAOException {
        LoanAccount loanAccount = null;
        try {
            String sql = "SELECT loan_accountid,account_number,account_balance,loan_rateid, principal_amount, monthly_payment, down_payment FROM account JOIN loan_account ON loan_account.account_id = account.account_id WHERE loan_account.account_id=?";

            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, accountId);

            ResultSet results = preparedStatement.executeQuery();

            logger.debug("Joining account information and account customer and customer successfully");

            if (results.next()) {

                int loanAccountId = results.getInt(1);
                String accountNumber = results.getString(2);
                double accountBalance = results.getDouble(3);
                int loanRateId = results.getInt(4);
                double principal = results.getDouble(5);
                double monthlyPayment = results.getDouble(6);
                double downPayment = results.getDouble(7);

                loanAccount = new LoanAccount();

                loanAccount.setAccountId(accountId);
                loanAccount.setLoanAccountId(loanAccountId);
                loanAccount.setAccountNumber(accountNumber);
                loanAccount.setAccountBalance(accountBalance);
                LoanTerm loanTerm = new LoanTerm();
                loanTerm.setLoanTermId(loanRateId);
                loanAccount.setLoanTerm(loanTerm);
                loanAccount.setPrincipal(principal);
                loanAccount.setMonthlyPayment(monthlyPayment);
                loanAccount.setDownPayment(downPayment);
            }

        } catch (SQLException e) {

            logger.error("Failed to find loan account by id", e);
            throw new DAOException("Failed to find loan account by id", e);
        }

        return loanAccount;
    }
}
