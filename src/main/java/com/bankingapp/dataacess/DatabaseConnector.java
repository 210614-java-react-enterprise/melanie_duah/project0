package com.bankingapp.dataacess;

import org.apache.logging.log4j.*;

import java.sql.*;
import java.sql.SQLException;
import java.sql.Connection;
import java.util.Map;

public class DatabaseConnector {
    private static Logger logger = LogManager.getLogger(DatabaseConnector.class);

    private static Connection connection = null;

    public static Connection getConnection() throws SQLException {

        if(connection == null) {
            Map<String,String> environment = System.getenv();
            String url = environment.get("databaseurl");
            String user = environment.get("user");
            String password = environment.get("password");
            String schema = environment.get("schema");
            connection = DriverManager.getConnection(url, user, password);
            connection.setSchema(schema);
            logger.debug("Creating connection to postgres");
        }

        return connection;
    }
}
