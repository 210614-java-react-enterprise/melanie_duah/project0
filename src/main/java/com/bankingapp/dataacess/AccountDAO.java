package com.bankingapp.dataacess;

import com.bankingapp.entities.*;
import com.bankingapp.entities.AccountType;

import java.util.List;

public interface AccountDAO {
    List<AccountType> getAllAccountTypes() throws DAOException;

    int getLastAcccountId() throws DAOException;

    void saveNewAccount(Account account, Customer... customers) throws DAOException;

    List<Account> findAccountsOfCustomer(Customer customer) throws DAOException;

    TransactionType getTransactionByName(String name) throws DAOException;

    void updateAccount(Account account) throws DAOException;

    void recordTransactions(List<AccountTransaction> transactions) throws DAOException;

    List<AccountTransaction> getAllTransactionByCustomer(Customer customer) throws DAOException;

    List<Account> findAllAccounts() throws DAOException;

    void deleteAccount(Account account) throws DAOException;

}
