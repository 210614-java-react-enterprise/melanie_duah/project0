package com.bankingapp.dataacess;

import com.bankingapp.entities.Account;
import com.bankingapp.entities.Customer;
import com.bankingapp.entities.LoanAccount;
import com.bankingapp.entities.LoanTerm;

import java.util.List;

public interface LoanDAO {
    List<LoanTerm> getLoanTerms() throws DAOException;

    void saveLoanAccount(Account account, LoanTerm loanTerm, double balance, double monthlyPayment, double downPayment) throws DAOException;
    List<LoanAccount> findLoanAccountsOfCustomer(Customer customer) throws DAOException;

    void saveLoanPayment(Customer customer, LoanAccount loanAccount, double paymentAmount) throws DAOException;

    LoanAccount findLoanAccountByAccountId(int loanAccountId) throws DAOException;
}
