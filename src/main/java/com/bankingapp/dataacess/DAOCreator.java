package com.bankingapp.dataacess;

import org.apache.logging.log4j.*;

import java.sql.SQLException;

public class DAOCreator {
    private static Logger logger = LogManager.getLogger(DAOCreator.class);

    private static AccountDAO accountDAO;
    private static CustomerDAO customerDAO;
    private static  LoanDAO loanDAO;

    public static AccountDAO getAccountDAO() throws DAOException {

        if (accountDAO == null) {
            logger.debug("Creating AccountDAO");
            try {
                accountDAO = new AccountDAOImpl(DatabaseConnector.getConnection()); //Connection created with DatabaseConnector factory and injected
            } catch (SQLException e) {
                throw new DAOException("Error Creating accountDAO", e);
            }
        }
        return accountDAO;
    }

    public static CustomerDAO getCustomerDAO() throws DAOException {
        if (customerDAO == null) {
            logger.debug("Creating CustomerDAO");
            try {
            customerDAO = new CustomerDAOImpl(DatabaseConnector.getConnection());
            } catch (SQLException e) {
                throw new DAOException("Error Creating customerDAO", e);
            }
        }
        return customerDAO;
    }

    public static LoanDAO getLoanDAO() throws DAOException{
        if (loanDAO == null) {
            logger.debug("Creating LoanDAO");
            try {
                loanDAO = new LoanDAOImpl(DatabaseConnector.getConnection());
            } catch (SQLException e) {
                throw new DAOException("Error Creating loanDAO", e);
            }
        }
        return loanDAO;
    }
}
