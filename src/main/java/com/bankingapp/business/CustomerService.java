package com.bankingapp.business;

import com.bankingapp.entities.*;

import java.util.List;

public interface CustomerService {
    Customer createNewCustomer(String userName, String password, String name, String email, String address) throws BusinessException;

    Customer findCustomer(String userName, String password) throws BusinessException;

    List<Customer> findAllCustomers() throws BusinessException;

    List<Customer> findCustomersBy(Account account) throws BusinessException;

    List<EmploymentStatus> getAllEmploymentStatus() throws BusinessException;

    void saveCustomerEmployment(Customer customer, EmploymentStatus employmentStatus,
                                String employerName, String jobTitle, int employmentDuration,
                                double annualIncome, String phoneNumber) throws BusinessException;
}
