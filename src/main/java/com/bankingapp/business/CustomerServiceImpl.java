package com.bankingapp.business;


import com.bankingapp.dataacess.*;

import com.bankingapp.entities.*;
import org.apache.logging.log4j.*;

import java.util.List;

public class CustomerServiceImpl implements CustomerService {
    private Logger logger = LogManager.getLogger(CustomerServiceImpl.class);
    private CustomerDAO customerDAO;
    public CustomerServiceImpl(CustomerDAO customerDAO) {
        this.customerDAO = customerDAO;
    }

    @Override
    public Customer createNewCustomer(String userName, String password, String name, String email, String address)
            throws BusinessException {
        Customer customer = null;
        try {
            customerDAO.saveCustomer(userName, password, name, email, address);
            customer = findCustomer(userName, password);
            logger.debug("Customer find successfully");

        } catch (DAOException e) {
            logger.error("failed to create a new customer");
            throw new BusinessException("failed to create new customer", e);
        }
        return customer;
    }

    @Override
    public Customer findCustomer(String userName, String password) throws BusinessException {
        Customer customer = null;
        try {
            customer = customerDAO.findCustomer(userName, password);
            logger.debug("Customer find successfully");

        } catch (DAOException e) {
            e.printStackTrace();
            logger.debug("failed to find customer");
            throw new BusinessException("failed to find customer", e);
        }
        return customer;
    }

    @Override
    public List<Customer> findAllCustomers() throws BusinessException {
        List<Customer> customers = null;
        try {
            customers = customerDAO.findAllCustomers();
            logger.debug("Customer find successfully");
        } catch (DAOException e) {
            e.printStackTrace();
            logger.debug("failed to find customer");
            throw new BusinessException("failed to find customers", e);
        }
        return customers;
    }

    @Override
    public List<Customer> findCustomersBy(Account account) throws BusinessException {
        List<Customer> customers = null;
        try {
            customers = customerDAO.findCustomersBy(account);
            logger.debug("successfully find customer by account");
        } catch (DAOException e) {
            e.printStackTrace();
            logger.error("failed to find customer by account", e);
            throw new BusinessException("failed to find customers by account", e);
        }
        return customers;
    }

    public List<EmploymentStatus> getAllEmploymentStatus() throws BusinessException {
        List<EmploymentStatus> employmentStatuses = null;
        try {
            employmentStatuses = customerDAO.getEmploymentStatus();
            logger.debug("successfully selected employment status");

        } catch (DAOException e) {
            e.printStackTrace();
            logger.error("failed to select employment status", e);
            throw new BusinessException("failed to select employment status", e);
        }
        return employmentStatuses;
    }

    @Override
    public void saveCustomerEmployment(Customer customer, EmploymentStatus employmentStatus,
                                       String employerName, String jobTitle, int employmentDuration,
                                       double annualIncome, String phoneNumber) throws BusinessException {

        try {
            CustomerEmployment customerEmployment = new CustomerEmployment();
            customerEmployment.setCustomer(customer);
            customerEmployment.setEmploymentStatus(employmentStatus);
            customerEmployment.setEmployerName(employerName);
            customerEmployment.setJobTitle(jobTitle);
            customerEmployment.setEmploymentDuration(employmentDuration);
            customerEmployment.setAnnualIncome(annualIncome);
            customerEmployment.setPhoneNumber(phoneNumber);

            customerDAO.saveCustomerEmployment(customerEmployment);
        } catch (DAOException e) {
            logger.error("failed to save employment", e);
            throw new BusinessException("failed to save customer employment", e);
        }
    }

}
