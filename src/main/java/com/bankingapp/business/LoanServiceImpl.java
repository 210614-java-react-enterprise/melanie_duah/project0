package com.bankingapp.business;

import com.bankingapp.dataacess.AccountDAO;
import com.bankingapp.dataacess.DAOException;
import com.bankingapp.dataacess.LoanDAO;
import com.bankingapp.entities.Account;
import com.bankingapp.entities.Customer;
import com.bankingapp.entities.LoanAccount;
import com.bankingapp.entities.LoanTerm;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class LoanServiceImpl implements LoanService{
    private LoanDAO loanDAO;
    private AccountDAO accountDAO;
    private Logger logger = LogManager.getLogger(CustomerServiceImpl.class);

    public LoanServiceImpl(LoanDAO loanDAO, AccountDAO accountDAO){
        this.loanDAO = loanDAO;
        this.accountDAO = accountDAO;
    }
    @Override
    public List<LoanTerm> getLoanTerms() throws BusinessException {
        List<LoanTerm> loanTerms = null;
        logger.debug("loan terms received");
        try{
            loanTerms = loanDAO.getLoanTerms();
        } catch (DAOException e) {
            logger.error("failed to get Loan terms and rate", e);
            throw new BusinessException("Failed to get loan terms and rate", e);
        }
        return loanTerms;
    }

    @Override
    public void saveLoanAccount(Account account, LoanTerm loanTerm, double balance, double monthlyPayment, double downPayment)
            throws BusinessException {
        try{
            loanDAO.saveLoanAccount(account, loanTerm, balance, monthlyPayment, downPayment);
        } catch (DAOException e) {
            logger.error("Failed to save loan account", e);
            throw new BusinessException("Failed to save loan account", e);
        }
    }

    @Override
    public List<LoanAccount> findLoanAccountsOfCustomer(Customer customer) throws BusinessException {
        List<LoanAccount> loanAccounts = null;
        logger.debug("Find loan accounts of customer");

        try{
            loanAccounts = loanDAO.findLoanAccountsOfCustomer(customer);
        } catch (DAOException e) {
            logger.error("Failed to get loan accounts of customer", e);
            throw new BusinessException("Failed to get loan accounts of customer", e);
        }
        return loanAccounts;
    }

    @Override
    public void savePayment(Customer customer, LoanAccount loanAccount, double paymentAmount) throws BusinessException {
        try{
            double balance = loanAccount.getAccountBalance() - paymentAmount;
            loanAccount.setAccountBalance(balance);

            accountDAO.updateAccount(loanAccount);
            loanDAO.saveLoanPayment(customer, loanAccount, paymentAmount);
        } catch (DAOException e) {
            logger.error("Failed to save loan payment of customer", e);
            throw new BusinessException("Failed to save loan payment of customer", e);
        }
    }

    @Override
    public void savePayment(Customer customer, Account account, double paymentAmount) throws BusinessException {
        try {
            LoanAccount loanAccount = loanDAO.findLoanAccountByAccountId(account.getAccountId());
            savePayment(customer, loanAccount, paymentAmount);
        } catch (DAOException e) {
            logger.error("Failed to save loan payment of customer", e);
            throw new BusinessException("Failed to save loan payment of customer", e);
        }
    }
}
