package com.bankingapp.business;

import com.bankingapp.dataacess.DAOCreator;
import com.bankingapp.dataacess.DAOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BusinessServiceCreator {
    private static Logger logger = LogManager.getLogger(BusinessServiceCreator.class);

    //factory method that creates CustomerService
    public static CustomerService getCustomerService() throws BusinessException {
        CustomerService customerService = null;
        logger.debug("Creating Customer Service");
        try {
            customerService = new CustomerServiceImpl(DAOCreator.getCustomerDAO()); //DaoCreator factory to get the customerDAO and inject it into CustomerServiceImpl
        } catch (DAOException e) {
            throw new BusinessException("Error creating CustomerService", e);
        }

        return customerService;
    }

    public static AccountService getAccountService() throws BusinessException {
        AccountService accountService = null;
        logger.debug("Creating AccountService Service");
        try {
            accountService = new AccountServiceImpl(DAOCreator.getAccountDAO());
        } catch (DAOException e) {
            throw new BusinessException("Error creating AccountService", e);
        }

        return accountService;
    }

    public static LoanService getLoanService() throws BusinessException {
        LoanService loanService = null;

        logger.debug("Creating LoanService Service");
        try {
            loanService = new LoanServiceImpl(DAOCreator.getLoanDAO(), DAOCreator.getAccountDAO());
        } catch (DAOException e) {
            throw new BusinessException("Error creating LoanService", e);
        }

        return loanService;
    }
}
