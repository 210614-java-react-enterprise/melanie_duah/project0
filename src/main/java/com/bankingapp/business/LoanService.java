package com.bankingapp.business;

import com.bankingapp.entities.Account;
import com.bankingapp.entities.Customer;
import com.bankingapp.entities.LoanAccount;
import com.bankingapp.entities.LoanTerm;

import java.util.List;

public interface LoanService {
    List<LoanTerm> getLoanTerms() throws BusinessException;
    void saveLoanAccount(Account account, LoanTerm loanTerm, double balance, double monthlyPayment, double downPayment)
            throws BusinessException;
    List<LoanAccount> findLoanAccountsOfCustomer(Customer customer) throws BusinessException;

    void savePayment(Customer customer, LoanAccount loanAccount, double paymentAmount) throws BusinessException;
    void savePayment(Customer customer, Account account, double paymentAmount) throws BusinessException;
}
