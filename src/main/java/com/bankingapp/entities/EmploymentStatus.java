package com.bankingapp.entities;

public class EmploymentStatus {
    private int employmentStatusId;
    private String employmentStatusName;

    public void setEmploymentStatusName(String employmentStatusName) {
        this.employmentStatusName = employmentStatusName;
    }

    public int getEmploymentStatusId() {
        return employmentStatusId;
    }

    public String getEmploymentStatusName() {
        return employmentStatusName;
    }

    public void setEmploymentStatusId(int employmentStatusId) {
        this.employmentStatusId = employmentStatusId;
    }
}
