package com.bankingapp.entities;

public class LoanTerm {
    private int loanTermId;
    private  int term;
    private double interestRate;

    public int getLoanTermId() {
        return loanTermId;
    }

    public void setLoanTermId(int loanTermId) {
        this.loanTermId = loanTermId;
    }

    public int getTerm() {
        return term;
    }

    public void setTerm(int term) {
        this.term = term;
    }

    public double getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(double interestRate) {
        this.interestRate = interestRate;
    }
}
