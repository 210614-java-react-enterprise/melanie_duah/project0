package com.bankingapp.entities;

import java.time.LocalDateTime;

public class LoanPayment {
    private int loanPaymentId;
    private  Customer customer;
    private LoanAccount loanAccount;
    private double amount;
    private LocalDateTime date;

    public int getLoanPaymentId() {
        return loanPaymentId;
    }

    public void setLoanPaymentId(int loanPaymentId) {
        this.loanPaymentId = loanPaymentId;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public LoanAccount getLoanAccounts() {
        return loanAccount;
    }

    public void setLoanAccounts(LoanAccount loanAccount) {
        this.loanAccount = loanAccount;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }
}
