package com.bankingapp.entities;

public class TransactionType {
    private int transactionTypeId;
    private String transactionTypeName;

    public int getTransactionTypeId() {
        return transactionTypeId;
    }

    public void setTransactionTypeId(int transactionTypeId) {
        this.transactionTypeId = transactionTypeId;
    }

    public String getTransactionTypeName() {
        return transactionTypeName;
    }

    public void setTransactonTypeName(String transactionTypeName) {
        this.transactionTypeName = transactionTypeName;
    }
}
