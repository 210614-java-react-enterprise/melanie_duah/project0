package com.bankingapp.entities;

public class LoanAccount extends Account {
    private int loanAccountId;
    private LoanTerm loanTerm;
    private double principal;
    private double monthlyPayment;
    private double downPayment;

    public int getLoanAccountId() {
        return loanAccountId;
    }

    public void setLoanAccountId(int loanAccountId) {
        this.loanAccountId = loanAccountId;
    }

    public LoanTerm getLoanTerm() {
        return loanTerm;
    }

    public void setLoanTerm(LoanTerm loanTerm) {
        this.loanTerm = loanTerm;
    }

    public double getPrincipal() {
        return principal;
    }

    public void setPrincipal(double principal) {
        this.principal = principal;
    }

    public double getMonthlyPayment() {
        return monthlyPayment;
    }

    public void setMonthlyPayment(double monthlyPayment) {
        this.monthlyPayment = monthlyPayment;
    }

    public double getDownPayment() {
        return downPayment;
    }

    public void setDownPayment(double downPayment) {
        this.downPayment = downPayment;
    }
}
