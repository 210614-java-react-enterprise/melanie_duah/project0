package com.bankingapp.entities;

public class CustomerEmployment {
    private int customerEmploymentId;
    private Customer customer;
    private EmploymentStatus employmentStatus;
    private String employerName;
    private  String phoneNumber;

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public EmploymentStatus getEmploymentStatus() {
        return employmentStatus;
    }

    public void setEmploymentStatus(EmploymentStatus employmentStatus) {
        this.employmentStatus = employmentStatus;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmployerName() {
        return employerName;
    }

    public void setEmployerName(String employerName) {
        this.employerName = employerName;
    }

    private int employmentDuration;
    private double annualIncome;
    private String jobTitle;

    public void setCustomerEmploymentId(int customerEmploymentId) {

        this.customerEmploymentId = customerEmploymentId;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public void setEmploymentDuration(int employmentDuration) {

        this.employmentDuration = employmentDuration;
    }

    public void setAnnualIncome(double annualIncome) {

        this.annualIncome = annualIncome;
    }

    public int getEmploymentDuration() {

        return employmentDuration;
    }

    public double getAnnualIncome() {
        return annualIncome;
    }
}
