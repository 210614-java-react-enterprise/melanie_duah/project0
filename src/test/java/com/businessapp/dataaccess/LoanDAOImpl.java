package com.businessapp.dataaccess;

import com.bankingapp.dataacess.AccountDAOImpl;
import com.bankingapp.dataacess.DAOException;
import com.bankingapp.dataacess.LoanDAO;
import com.bankingapp.entities.AccountType;
import org.junit.jupiter.api.*;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class LoanDAOImpl {

    @Mock
    Connection connection;

    @Mock
    PreparedStatement preparedStatement;

    @Mock
    ResultSet results;

    public LoanDAOImpl(Connection connection) {
        this.connection=connection;
    }

    @BeforeEach
    void initializeTest() throws DAOException, SQLException {
        MockitoAnnotations.openMocks(this);
        when(connection.prepareStatement(any(String.class))).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(results);
        when(results.getInt(any(int.class))).thenReturn((new Random().nextInt(5) + 1));
        when(results.getString(any(int.class))).thenReturn("test string");
        when(results.getTimestamp(any(int.class))).thenReturn(java.sql.Timestamp.valueOf(LocalDateTime.now()));
        when(results.next()).thenReturn(true, true, true, false);
    }
    @Test
    @DisplayName("getAll Loan Terms should return a list of Loan Terms")
    void getLoanTerms() throws DAOException {
        // Arrange
        LoanDAOImpl loanDAO = new LoanDAOImpl(connection);

        // Act


        // Assert

    }
}
