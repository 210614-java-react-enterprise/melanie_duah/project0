# Banking and Loan Administration Application

## Project Description

The banking application allows banking customers to create and operate various types of accounts including checking, savings and loans. Customers can perform various operations on each account including account creation, deposits, withdrawals and transfers. 

The project is mainly written in Java with a Postgres SQL database. Users interact with the system using a command line interface.

## Technologies Used

* Postgres SQL database - version 13
* PostgresSQL JDBC Driver - 42.2.20.jre7
* Log4J - version 2.14.1
* JUnit Jupiter - version 5.7.1
* Mockito - version 3.9.0
* Apache Maven for dependency management
* Jetbrains IntelliJ IDEA 2021.2 Community Edition

## Features
* Customers can create user accounts for log in access to the system
* Customers can create individual or joint accounts
* Account types supported include Savings, Checking and Loan each having individual or joint variants
* Customers can deposit, withdraw, or transfer funds from accounts.

To-do list:
* Provide a Graphical User Interface 
* Allow other types of users with different roles and access

## Getting Started
   
* > git clone https://gitlab.com/210614-java-react-enterprise/melanie_duah/project0.git
* > Install JDK version 16+
* > Install an IDE - IntelliJ IDEA (recommended as this is what is used for this project), Eclipse or Netbeans


## Usage
Build and run the application. Here is what the basic command line interface looks like


## License

This project uses the following license: [APACHE LICENSE, VERSION 2.0](<https://www.apache.org/licenses/LICENSE-2.0.txt>)

